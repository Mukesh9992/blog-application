import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
import { AppRoutingModule } from './/app-routing.module';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { HeroService } from './hero.service';
import { DetailComponent } from './detail/detail.component';
import { LoginComponent } from './login/login.component';
import { InputComponent } from './input/input.component';
import {HttpClientModule} from '@angular/common/http';
import { UrlService } from './url.service';
import { NavigationComponent } from './navigation/navigation.component';
import { CommentsComponent } from './comments/comments.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';



@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    OneComponent,
    TwoComponent,
    DetailComponent,
    LoginComponent,
    InputComponent,
    NavigationComponent,
    CommentsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [HeroService,UrlService,/*LocaldataService*/],
  bootstrap: [AppComponent]
})
export class AppModule { }
